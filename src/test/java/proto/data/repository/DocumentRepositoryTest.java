package proto.data.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;
import proto.data.App;
import proto.data.model.Document;
import proto.data.model.DocumentSpecs;
import proto.data.model.DocumentView;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class DocumentRepositoryTest {

    @Autowired
    private DocumentRepository documentRepository;

    @Test
    public void specificationWithProjection() {
        Specifications<Document> where = Specifications.where(DocumentSpecs.idEq(1L));
        Page<DocumentView> all = documentRepository.findAll(where, DocumentView.class, new PageRequest(0,10));
        Assertions.assertThat(all).isNotEmpty();
    }
}
