package proto.data.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Document {
    @Id
    private Long id;

//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private Owner owner;

    private String description;
    private String documentType;
    private String documentCategory;
}
