package proto.data.model;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class DocumentSpecs implements Specification<Document> {

    private Document filter;

    public static Specification<Document> idEq(Long id){

        return (root, query, cb) -> cb.equal(root.get("id"), id);
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder cb) {
        Predicate p = cb.disjunction();

        if (filter.getId() != null) {
            p.getExpressions()
                    .add(cb.equal(root.get("id"), filter.getId()));
        }

        return p;
    }
}
