package proto.data.model;

import lombok.Data;

public interface DocumentView {
    Long getId();
    String getDescription();
}
