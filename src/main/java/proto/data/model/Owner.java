package proto.data.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Owner {
    @Id
    private Long id;

    private String name;
    private String division;
}
