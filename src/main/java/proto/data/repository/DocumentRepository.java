package proto.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import proto.data.model.Document;
import th.co.geniustree.springdata.jpa.repository.JpaSpecificationExecutorWithProjection;

public interface DocumentRepository extends JpaRepository<Document,Long>, JpaSpecificationExecutorWithProjection<Document> {
}
