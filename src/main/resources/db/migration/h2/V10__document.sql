CREATE TABLE DOCUMENT(
    id                 BIGINT GENERATED BY DEFAULT AS IDENTITY,
    owner_id           BIGINT,
    description        VARCHAR(255),
    document_type      VARCHAR(255),
    document_category  VARCHAR(255),
    PRIMARY KEY (ID)
);

INSERT INTO DOCUMENT VALUES (1, 1, 'Description', 'Type', 'Category');
